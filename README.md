# Fullstack Test Sheet

## Getting started

Welcome to UDT Full-stack test sheet. This test has 3 pages.
And you need to setup the frontend, backend and infrastructure for the code base.
It depends on your choice if you want to divide the source into 3 repositories or put all in one place.
Please try to finish the test within 5 days.

## Technology:

### Frontend
You need to use [Next.js](https://nextjs.org/) to setup the frontend part.
You can use any UI frameworks such as Bootstrap, Tailwind, etc to setup the styling.

### Backend
You need to use [Loopback 4](https://loopback.io/index.html) to setup the backend part.
You need to use [MongoDB](https://www.mongodb.com/) to setup the database.

### Infrastructure
You need to use [Terraform](https://www.terraform.io/) to setup the infrastructure part.

## Note

The above technologies are the stack that we are using.
In case of you can't use one of them, please use any alternative solution you are familiar with.
When you create database schema, please use [PlantUML](https://plantuml.com/) to draw the [Entity Relationship Diagram](https://plantuml.com/ie-diagram).

## Requirement
You need to create a small web application including 3 pages and auth page.

- The auth page allows a user to login with an account and sign up a new account. When the system initiate, please create an admin account.
An admin account who can see all figures. On the other side, normal users can only see their figures.

- The first page is an `Input page` that allows the user to input some information such as symbol, shape, color and measurement.
Symbol field is the symbol that the app uses to draw the figure.
Shape field is the shape of the figure that the app draw. The shape can be a diamond, perfect triangle or rectangle.
Color field is the color of the symbol.
Measurement field is the height of a diamond, the height of the perfect triangle or the edge of the rectangle.
There is 2 buttons which `List` and `Draw` in the first page.
When a user clicks on the `List` button, it navigates the user to `List page`.
When a user clicks on the `Draw` button, it navigates the user to `Draw page`.

- The second page is a `Draw page` that draws a figure basing on the input data.
i.e: symbol: L , shape: perfect triangle, color: FFFFFF, measurement: 5

the "-" is only to help render the figure in markdown file.

----L

---LLL

--LLLLL

-LLLLLLL

LLLLLLLLL

i.e: symbol: A, shape: diamond, color: FFFFFF, measurement: 7

----A

---AAA 

--AAAAA

AAAAAAA

--AAAAA

---AAA

----A

i.e: symbol: G, shape: rectangle, color: FFFFFF, measurement: 6

GGGGGG

GGGGGG

GGGGGG

GGGGGG

GGGGGG

GGGGGG

There is 2 buttons which `Input` and `List` in the second page.
When a user clicks on the `List` button, it navigates the user to `List page`.
When a user clicks on the `Input` button, it navigates the user to `Input page`.

- The third page is a `List page` that lists all history of figures.
There is a table in a list page, each row display the input of previous figures.
Click on a row of figure, it navigates the user to the `Draw page` and draw that figure.
There is 1 button which `Input` in the third page.
When a user clicks on the `Input` button, it navigates the user to the `Input page`.

[Requirement wireframe](https://www.figma.com/file/ClZ08iXGCWgUA6xb6Rwo9Z/UDT-Test-Screens)
